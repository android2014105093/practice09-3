package sound1.ex.vr.ex_sound1;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

public class Act1 extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener{

    private MediaPlayer mPlayer = null;
    private SeekBar mSeekBar;
    private ProgressBar mProgress;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act1);

        mPlayer = MediaPlayer.create(this, R.raw.music);
        initMP();
    }

    @Override
    protected void onDestroy() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
        super.onDestroy();
    }

    public void initMP() {
        mPlayer = MediaPlayer.create(this, R.raw.music);

        mProgress = (ProgressBar) findViewById(R.id.progressBar1);
        mProgress.setProgress(0);
        mProgress.setMax(0);

        mSeekBar = (SeekBar) findViewById(R.id.seekBar);
        mSeekBar.setOnSeekBarChangeListener(this);
        mSeekBar.setProgress(0);
        mSeekBar.setMax(0);

        ProThread thrd = new ProThread();
        thrd.setDaemon(true);
        thrd.start();
    }

    public void startMP(View v) {
        Button btn;
        btn = (Button) findViewById(R.id.button1); // 각 버튼의 상황별 Enable 결정
        btn.setEnabled(false);
        btn = (Button) findViewById(R.id.button2);
        btn.setEnabled(true);

        mProgress.setProgress(0);
        mProgress.setMax(mPlayer.getDuration());

        mSeekBar.setProgress(0);
        mSeekBar.setMax(mPlayer.getDuration());

        mPlayer.start();
        mPlayer.setLooping(true);
    }

    public void pauseMP(View v) {
        if (mPlayer.isPlaying()) {
            mPlayer.pause();
        } else {
            mPlayer.start();
        }
    }

    public void stopMP(View v) {
        mPlayer.stop();
        initMP();
    }

    public void showProgress() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mProgress.setProgress(mPlayer.getCurrentPosition());
            mSeekBar.setProgress(mPlayer.getCurrentPosition());
            TextView output = (TextView)findViewById(R.id.textView1);
            output.setText(
                    "재생 중"
                            + "\n현재 위치: " + mPlayer.getCurrentPosition() / 1000
                            + "\n전체 길이: " + mPlayer.getDuration() / 1000 );
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        mPlayer.seekTo(i);
        mProgress.setProgress(i);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

     class ProThread extends Thread {
        public Handler mHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    showProgress();
                }
            }
        };

        public void run() {
            while (mPlayer != null) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
                mHandler.sendMessage(Message.obtain(mHandler, 1));
            }
        }
    }
}

